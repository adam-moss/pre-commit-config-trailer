# Pre-Commit-Trailer

[![Release][repo-tag-image]][repo-url]
[![License][license-image]][license-url]

A [git] hook for [pre-commit] to add a trailer into a commit message indicating the hook has been executed.

> **Warning:**
>
> The presence of the `Pre-Commit-Config:` trailer does not guarantee any hooks have been executed. This must be factored into your threat model if making any assertions based upon its presence.

## Using Pre-Commit-Trailer with Pre-Commit

Add this to your `.pre-commit-config.yaml`

```yaml
- repo: https://gitlab.com/adam-moss/pre-commit-trailer
  rev: v1.0.0 # Use the ref you want to point at
  hooks:
    - id: add-pre-commit-config-trailer
  # -   id: ...
```

## Hooks Available

### `add-pre-commit-config-trailer`

![commit-msg hook][hook-commit-msg-image]

Add a `Pre-Commit-Config:` commit message trailer with a sha256 hash of the executed `.pre-commit-config.yaml`

- the hash is generated with `shasum --algorithm 256 --UNIVERSAL`
- the resultant `U.pre-commit-config.yaml` part is removed

### `add-pre-commit-user-skipped-trailer`

![commit-msg hook][hook-commit-msg-image]

Add a `Pre-Commit-User-Skipped:` commit message trailer with the name of each skipped hook.

[git]: https://www.git-scm.org
[hook-commit-msg-image]: https://img.shields.io/badge/hook-commit--msg-informational?logo=git
[license-image]: https://img.shields.io/gitlab/license/adam-moss/pre-commit-trailer
[license-url]: https://opensource.org/licenses/ISC
[pre-commit]: https://www.pre-commit.com
[repo-tag-image]: https://img.shields.io/gitlab/v/tag/adam-moss/pre-commit-trailer?include_prereleases&sort=semver
[repo-url]: https://gitlab.com/adam-moss/pre-commit-trailer
